// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import App from './App'

// https://www.npmjs.com/package/vue-axios
import axios from 'axios' //主要AJAX套件 
import VueAxios from 'vue-axios' //轉為VUE套件 可以直接用THIS用他

Vue.use(VueAxios, axios)
Vue.config.productionTip = false

/* eslint-disable no-new */
new Vue({
  el: '#app',
  components: { App },
  template: '<App/>'
})
